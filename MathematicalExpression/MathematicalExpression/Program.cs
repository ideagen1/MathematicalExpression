﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MathematicalExpression
{
    internal class Program
    {
        static void Main(string[] args)
        {
            var expressionList = new List<string>() {
            "1 + 1",
            "2 * 2",
            "1 + 2 + 3",
            "6 / 2",
            "11 + 23",
            "11.1 + 23",
            "1 + 1 * 3",
            "( 11.5 + 15.4 ) + 10.1",
            "23 - ( 29.3 - 12.5 )",
            "( 1 / 2 ) - 1 + 1",
            "10 - ( 2 + 3 * ( 7 - 5 ) )", //Additional onwards
            "( 5 + 5 )",
            "10 - ( ( 4 + 4 ) + ( 4 + 4 ) )",
            "( ( 4 * 4 ) - 10 )",
            "( ( 4 - 2 + 1 ) * ( ( 2 + 2 ) + ( 3 + 3 - 3 ) ) )"
            };

            foreach (var expression in expressionList)
            {
                try
                {
                    Console.WriteLine(expression + " : " + Calculate(expression));
                }
                catch(Exception ex)
                {
                    Console.WriteLine(ex.Message.ToString());
                }
            }
            Console.ReadLine();
        }

        private static double Calculate(string expression)
        {
            var finalValue = 0.00D;
            var firstExpressionList = new List<string>();
            var secondExpressionList = new List<string>();
            var newExpressionList = new List<string>();
            var expressionList = expression.Split(' ').ToList();

            if (expressionList.IndexOf("(") != -1)
            {
                var bracketOccurenceCount = expressionList.Where(x => x.Contains("(")).Count();

                //only one brack expression
                if (bracketOccurenceCount == 1)
                {
                    var startBracketIndex = expressionList.IndexOf("(");
                    var endBracketIndex = expressionList.LastIndexOf(")");
                    var bracketExpressionList = new List<string>();

                    var lengthCount = endBracketIndex - startBracketIndex - 1;
                    bracketExpressionList = expressionList.GetRange(startBracketIndex + 1, lengthCount);

                    //whole expression is bracket itself
                    if (startBracketIndex == 0 && endBracketIndex == expressionList.Count() - 1)
                        finalValue = ExpressionSequenceProcessing(bracketExpressionList);
                    else
                    {
                        var processValue = ExpressionSequenceProcessing(bracketExpressionList);

                        //start with bracket expression in equation
                        if (startBracketIndex == 0)
                        {
                            firstExpressionList.Add(processValue.ToString());
                            secondExpressionList = expressionList.GetRange(endBracketIndex + 1, expressionList.Count() - endBracketIndex - 1);
                            newExpressionList.AddRange(firstExpressionList);
                            newExpressionList.AddRange(secondExpressionList);
                        }
                        else
                        {
                            firstExpressionList = expressionList.GetRange(0, startBracketIndex);
                            secondExpressionList.Add(processValue.ToString());
                            newExpressionList.AddRange(firstExpressionList);
                            newExpressionList.AddRange(secondExpressionList);

                            //if it's not end of the equation
                            if (endBracketIndex != expressionList.Count() - 1)
                            {
                                var thirdExpressionList = expressionList.GetRange(endBracketIndex + 1, expressionList.Count() - endBracketIndex - 1);
                                newExpressionList.AddRange(thirdExpressionList);
                            } 
                        }
                        finalValue = ExpressionSequenceProcessing(newExpressionList);
                    }
                }
                else
                {
                    var currentNewExpressionList = new List<string>();
                    var startBracketIndex = 0;
                    var endBracketIndex = 0;

                    for (int i = 0; i < expressionList.Count(); i++)
                    {
                        if (expressionList[i] == "(")
                            startBracketIndex = i;
                        else if (expressionList[i] == ")")
                        {
                            endBracketIndex = i;
                            break;
                        }
                    }

                    var lengthCount = endBracketIndex - startBracketIndex - 1;
                    var bracketExpressionList = expressionList.GetRange(startBracketIndex + 1, lengthCount);
                    var processValue = ExpressionSequenceProcessing(bracketExpressionList);

                    if (startBracketIndex == 0)
                    {
                        firstExpressionList = new List<string> { processValue.ToString() };
                        secondExpressionList = expressionList.GetRange(endBracketIndex + 1, expressionList.Count() - endBracketIndex - 1);

                        currentNewExpressionList.AddRange(firstExpressionList);
                        currentNewExpressionList.AddRange(secondExpressionList);
                    }
                    else
                    {
                        firstExpressionList = expressionList.GetRange(0, startBracketIndex);
                        secondExpressionList = new List<string> { processValue.ToString() };

                        currentNewExpressionList.AddRange(firstExpressionList);
                        currentNewExpressionList.AddRange(secondExpressionList);

                        if (endBracketIndex != expressionList.Count() - 1)
                        {
                            var thirdExpressionList = expressionList.GetRange(endBracketIndex + 1, expressionList.Count() - endBracketIndex - 1);
                            currentNewExpressionList.AddRange(thirdExpressionList);
                        }
                    }
                    var finalExpression = string.Join(" ", currentNewExpressionList);
                    return Calculate(finalExpression);
                }
            }
            else
                finalValue = ExpressionSequenceProcessing(expressionList);

            return finalValue;
        }

        private static double ExpressionSequenceProcessing(List<string> expressionList)
        {
            var finalValue = 0.00D;
            var firstExpressionList = new List<string>();
            var secondExpressionList = new List<string>();
            var newExpressionList = new List<string>();

            var firstNumberIndex = 0;
            var operatorIndex = 0;
            var secondNumberIndex = 0;

            //process times or divide first
            if (expressionList.Any(x => x.Contains("*") || x.Contains("/")))
            {
                foreach (var expression in expressionList)
                {
                    if (expression == "*" || expression == "/")
                        break;
                    operatorIndex++;
                }
                firstNumberIndex = operatorIndex - 1;
                secondNumberIndex = operatorIndex + 1;
            }
            else
            {
                firstNumberIndex = 0;
                operatorIndex = 1;
                secondNumberIndex = 2;
            }

            var toProcessExpression = expressionList.GetRange(firstNumberIndex, 3);
            var calculatedValue = MathematicalCalculation(toProcessExpression);
            if (expressionList.Count() == 3)
                finalValue = calculatedValue;
            else
            {
                firstExpressionList = expressionList.GetRange(0, firstNumberIndex);
                firstExpressionList.Add(calculatedValue.ToString());
                secondExpressionList = expressionList.GetRange(secondNumberIndex + 1, expressionList.Count() - secondNumberIndex - 1);
                newExpressionList.AddRange(firstExpressionList);
                newExpressionList.AddRange(secondExpressionList);
                expressionList = newExpressionList;
                return ExpressionSequenceProcessing(expressionList);
            }
            return finalValue;
        }

        private static double MathematicalCalculation(List<string> expressionList)
        {
            var firstNumberIndex = 0;
            var operatorIndex = 1;
            var secondNumberIndex = 2;

            var firstNumber = Convert.ToDouble(expressionList[firstNumberIndex]);
            var secondNumber = Convert.ToDouble(expressionList[secondNumberIndex]);

            var calculatedValue = 0.00D; 
            switch (expressionList[operatorIndex])
            {
                case "*":
                    calculatedValue = firstNumber * secondNumber;
                    break;
                case "/":
                    calculatedValue = firstNumber / secondNumber;
                    break;
                case "+":
                    calculatedValue = firstNumber + secondNumber;
                    break;
                case "-":
                    calculatedValue = firstNumber - secondNumber;
                    break;
                default:
                    calculatedValue = 0;
                    break;
            }
            return calculatedValue;
        }
    }
}
